module MEM_WB
(
	input clk,
	input reset,
	input enable,
	
	input [1:0] WB_in,
	input [31:0] ReadData_in,
	input [31:0] ALUResult_in,
	input [4:0] SelectedDest_in,
	
	output reg [1:0] WB_out,
	output reg [31:0] ReadData_out,
	output reg [31:0] ALUResult_out,
	output reg [4:0] SelectedDest_out

	);

always@( negedge clk ) begin
	if(reset==0) begin
		WB_out <= 0;
		ReadData_out <= 0;
		ALUResult_out <= 0;
		SelectedDest_out <= 0;
		end
	else	
		if(enable==1) begin
			WB_out <= WB_in;
			ReadData_out <= ReadData_in;
			ALUResult_out <= ALUResult_in;
			SelectedDest_out <= SelectedDest_in;
			end
end

endmodule
