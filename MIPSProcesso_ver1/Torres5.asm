#SP1 -> SP torre 1	
#SP2 -> SP torre 2
#SP3 -> SP torre 3

#$a0 -> Disco
#$a1 -> Origen
#$a2 -> Destino
#$a3 -> Aux

.data 
	.space 32
	Torre1: .space 64	#Reserva espacio para torres de (64/4) discos
	Torre2: .space 64
	Torre3: .space 64
 	#.space 32
	SP1: .space 4		#Reserva espacio para los apuntadores a torres
	SP2: Torre2
	SP3: Torre3 
.text

MAIN:
	lui $sp, 0x1001
	ori $sp, 0x800
	addi $t9, $zero, 8	# Se la cantidad de discos al registro $t9
	add $a0, $zero, $t9	# Se carga la cantidad de discos como parametro para la inicializacion 

#La rutina de inicializacion carga los discos en la torre 1, actualzia el valor del SP1 y carga los argumentos de la rutina hanoi
inicializacion:
	lui $t1, 4097		# La direccion de la torre1 es 10010000H, entonces se carga 1001 H en la parte alta de $t1 
	ori  $t1, $t1, 32	# Se carga la parte baja de la direccion de torre1. $t1 se usara para recorrer las direcciones
	
keep_carga:
	sw $a0, ($t1)		# Se carga el disco actual en la direccion a donde apunta $t1
	add $a0, $a0, -1	# Se decrementa el valor del disco actual
	add $t1, $t1, 4		# Se incrementa el valor de t1 para apuntar a la siguiente direccion, se aumenta en 4 porque un disco es una palabra, es  decir 4 bytes
	bnez $a0, keep_carga	# Si el valor del disco actual no ha llegado a cero, se repite el proceso para cargar el siguiente disco. Si ya llego a cero, se continua.
	
	#Ya cargados los discos en la memoria, se cargaran los argumentos para hanoi. 
	
	lui $a1, 4097		# Se carga la direccion del SP1 en el $a1.
	ori $a1, $a1, 224	# La direccion es 100100e0H, entonces se carga 1001H en la parte alta y 00e0H en la parte baja,
	
	sw $t1, 0($a1)		# $t1 termino con la direccion del tope de la pila, entonces se guarda en $a1
	
	add $s7, $zero, 2	# Para agilizar las comparaciones disco == 2 se utiliza el registro $s7, que conservara un 2 durante toda la ejecucion
	
	add $a0, $zero, $t9	# Se mueve la cantidad de discos al $a0 

	add $a2, $a1, 8		# Como los SP de las torres estan continuos en memoria, se carga en $a2 (destino) la direccion del SP2 (SP1 + 8) 
	lw $t0, ($a2)
	#lui $t1  
	add $a3, $a1, 4		# Se guarda en $a3 (auxiliar) la direccion del SP3 (SP1+4)
	
			
	addi $t3, $zero, 1	# El algoritmo contempla el caso base como el 2, entonces el caso N=1 debe manejarse aparte.
	beq $t3, $t9, UNO	# Si el numero de discos fue 1 salta a UNO
	
	jal hanoi		# Llamada a Hanoi
	

fin:	j fin			# Fin de programa, se detiene el programa aqui para no ejecutar las demas instrucciones





# La funcion de hanoi esta optimizada al tratar el caso base como el 2, ahorrandose todas las llamadas cuando el disco actual es 1.
# Esto se puede hacer porque la solucion cuando N=2 tambien es trivial, se mueve el disco 1 al auxiliar, luego el disco 2 al destino
# y finalmente el disco 1 se pone en el destino sobre el disco 2.

hanoi:

	bne $a0, $s7, recursive_case		#Al inicio de la funcion se compara el disco actual con 2, si no es igual se salta hasta el caso recursivo.
	
#base_case: El caso base mueve los discos 1 y 2 al destino utilizando el procedimiento anteriorment descrito.

	lw $t1, ($a1)		# Se obtiene el valor del apuntador a la torre de origen y se carga en $t1.
	lw $t2, ($a2)		# Se obtiene el valor del apuntador a la torre de destino y se carga en $t2.
	lw $t3, ($a3)		# Se obtiene el valor del apuntador a la torre auxiliar y se carga en $t3.
	

	lw $t4, -4($t1)		# Se obtiene el disco 1 de la torre de origen	
	sw $zero, -4($t1)	# Se quita el disco 1 de la torre de origen
	sw $t4, 0($t3)		# Se escribe el disco 1 en la torre auxiliar
	
	add $t1, $t1, -8 	# Se decrementa el apuntador de tope de la torre de origen en 2 posiciones
	
	sw $t1, 0($a1)		# Se escribe el nuevo valor del SP de origen en la memoria.
	
	lw $t4, 0($t1)		# Se obtiene el disco 2 de la torre de origen
	sw $zero, 0($t1)	# Se quita el disco 2 de la torre de origen
	sw $t4, 0($t2)		# Se escribe el disco 2 en la torrre de destino
	
	lw $t4, 0($t3)		# Se obtiene el disco 1 de la torre auxiliar 
	sw $zero, 0($t3)	# Se quita el disco 1 de la torre auxiliar
	sw $t4, 4($t2)		# Se escribe el disco 1 en la torre de destino
	
	add $t2, $t2, 8		# Se incrementa el apuntador de tope de la torre de destino en 2 posiciones
	sw $t2, 0($a2)		# Se escribe el nuevo valor de la SP de destino en la memoria
	
	jr $ra			# Se hace el backtrack
recursive_case:
	
#guardarEstado1: Para hacer la primera llamada recursiva se tiene que gudardar el estado actual la funcion. Los datos que se requieren guardar son
# las direcciones de los SP de origen, destino y auxliar, asi como la direccion de retorno de la ejecucion actual. El disco no e necesario guardarlo, 
# porque se sabe que va a decrementar al entrar e incrementar al salir.

	sw $a1, -8($sp)		# Se guarda la direccion de SP de origen
	sw $a2, -12($sp)	# Se guarda la direccion de SP de destino
	sw $a3, -16($sp)	# Se guarda la direccion de SP de auxiliar
	sw $ra, -20($sp)	# Se guarda la direccion de retorno.

#cargarparametros1:
	add $a0, $a0, -1	# Se decrementa el disco para la siguiente llamada
	lw $a2, -16($sp)	# Se hace swap entre auxiliar y destino, aprovechamos que los datos ya estan en el stack para intercambiarlos desde la memroria
	lw $a3, -12($sp)	
	add $sp, $sp, -20	# Se incrementa el $sp del programa
	
#llamadaRecursiva:
	jal hanoi
	
#recuperarEstado1: Despues de la siguiente llamada recursiva se requiere recuperar el entorno, pero no en su totalidad.
# Esto es porque la siguiente operacion es mover de origen a destino, entonces de momento solo se requiere restaurar $a1 y $a22

	lw $a2, 8($sp)		# Se recupera $a2.
	lw $a1, 12($sp)		# Se recupera $a1
	
#movimiento: Se hace el intercambio entre la torre de origen y de destino
	lw $t1, ($a1)		# Se obtiene la direccion a la que apunta SP de origen
	lw $t2, ($a2)		# Se obtiene la direccion a la que apunta SP de destino
	
	add $t1, $t1, -4	# Se decrementa el apuntador de pila de origen
	sw $t1, ($a1) 		# y se regresa a la memoria.
	
	lw $t4, 0($t1)		# Se obtiene un disco de la torre de origen
	sw $zero, 0($t1)	# Se quita el disco de la torre de origen
	sw $t4, 0($t2)		# Se escribe el disco en la torre de destino
	
	add $t2, $t2, 4		# Se escribe incrementa el apuntador de la torre de destino y se regresa a la memoria.
	sw $t2, 0($a2)
	
#guardarEstado2: Aunque se realizara otra llamada recursiva no es necerario guardar el estado del programa, debido a que en el stack continuan guardados 
# los datos del entorno antes de la llamada recursiva anterior.

#cargarparametros2: Para la siguiente llamada se tienen que intercambiar el auxiliar y el origen. Se aprovecha que ya se encuentran estos datos en el stack para cargalos
# en los argumentos correctos.

	lw $a1, 4($sp)	#move $a1, $a3
	lw $a3, 12($sp) #move $a3, $t0
	
	jal hanoi	#salto 2
	
#recuperarEstado1: Despues de la ultima llamada recursiva, no es necesario volver a recuperar todo el entorno, solo lo suficiente para las llamadas que se se encuentran en 
# el stack. Se requiere regresar el valor del disco a su estao anterior y decrementar el stack. Tambien se recupera la direccion de retorno
	lw $ra, 0($sp)
	add $a0, $a0, 1		#Para regresar el disco a su valor anterior solo se incrementa en 1.
	add $sp, $sp, 20
	
	jr $ra

UNO: 
	lw $t1, ($a1)		# Se obtiene la direccion a la que apunta SP de origen
	lw $t2, ($a2)		# Se obtiene la direccion a la que apunta SP de destino
	
	add $t1, $t1, -4	# Se decrementa el apuntador de pila de origen
	sw $t1, ($a1) 		# y se regresa a la memoria.
	
	lw $t4, 0($t1)		# Se obtiene el disco 1 disco de la torre de origen
	sw $zero, 0($t1)	# Se quita el disco 1 de la torre de origen
	sw $t4, 0($t2)		# Se escribe el disco 1 en la torre de destino
	
	add $t2, $t2, 4		# Se escribe incrementa el apuntador de la torre de destino y se regresa a la memoria.
	sw $t2, 0($a2)
	
	j fin
