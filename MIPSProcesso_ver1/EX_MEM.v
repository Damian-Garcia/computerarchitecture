module EX_MEM
(
	input clk,
	input reset,
	input enable,
	
	input [1:0] WB_in,
	input [3:0] M_in,
	input [31:0] BranchAddr_in,
	input ZeroFlag_in,
	input [31:0] ALUResult_in,
	input [31:0] WriteData_in,
	input [4:0] SelectedDest_in,
	
	output reg [1:0] WB_out,
	output reg [3:0] M_out,

	output reg [31:0] BranchAddr_out,
	output reg ZeroFlag_out,
	output reg [31:0] ALUResult_out,
	output reg [31:0] WriteData_out,
	output reg [4:0] SelectedDest_out
);

always@( negedge clk ) begin
	if(reset==0) begin
		WB_out <= 0;
		M_out <= 0;
		BranchAddr_out <= 0;
		ZeroFlag_out <= 0;
		ALUResult_out <= 0;
		WriteData_out <= 0;
		SelectedDest_out <= 0;
		end
	else	
		if(enable==1) begin
			WB_out <= WB_in;
			M_out <= M_in;
			BranchAddr_out <= BranchAddr_in;
			ZeroFlag_out <= ZeroFlag_in;
			ALUResult_out <= ALUResult_in;
			WriteData_out <= WriteData_in;
			SelectedDest_out <= SelectedDest_in;
		end
end

endmodule
