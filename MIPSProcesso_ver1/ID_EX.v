module ID_EX
(
	input clk,
	input reset,
	input enable,
	
	input [1:0] WB_in,
	input [3:0] M_in,
	input [4:0] EX_in,
	input [31:0] PC_4_in,
	input [31:0] readData1_in,
	input [31:0] readData2_in,
	input [31:0] extendedConstant_in,
	input [4:0] RegisterRS_in,
	input [4:0] RegisterRT_in,
	input [4:0] RegisterRD_in,
	input [4:0] Shamt_in,
	input [5:0] ALUFunc_in,
	
	
	output reg [1:0] WB_out,
	output reg [3:0] M_out,
	output reg [4:0] EX_out,
	output reg [31:0] PC_4_out,
	output reg [31:0] readData1_out,
	output reg [31:0] readData2_out,
	output reg [31:0] extendedConstant_out,
	output reg [4:0] RegisterRS_out,
	output reg [4:0] RegisterRT_out,
	output reg [4:0] RegisterRD_out,
	output reg [4:0] Shamt_out,
	output reg [5:0] ALUFunc_out	
	);
	
always@( negedge clk ) begin
	if(reset==0) begin
		WB_out <= 0;
		M_out <= 0;
		EX_out <= 0;

		PC_4_out <= 0;
		readData1_out <= 0;
		readData2_out <= 0;
		extendedConstant_out <= 0; 
		RegisterRS_out <= 0;
		RegisterRT_out <= 0;
		RegisterRD_out <= 0;
		Shamt_out <= 0;
		ALUFunc_out <= 0;

		end
	else	
		if(enable==1) begin
			WB_out <= WB_in;
			M_out <= M_in;
			EX_out <= EX_in;

			PC_4_out <= PC_4_in;
			readData1_out <= readData1_in;
			readData2_out <= readData2_in;
			extendedConstant_out <= extendedConstant_in; 
			RegisterRS_out <= RegisterRS_in;
			RegisterRT_out <= RegisterRT_in;
			RegisterRD_out <= RegisterRD_in;
			Shamt_out <= Shamt_in;
			ALUFunc_out <= ALUFunc_in;
		end
end


endmodule
