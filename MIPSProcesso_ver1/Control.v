/******************************************************************
* Description
*	This is control unit for the MIPS processor. The control unit is 
*	in charge of generation of the control signals. Its only input 
*	corresponds to opcode from the instruction.
*	1.0
* Author:
*	Dr. JosÃƒÂ© Luis Pizano Escalante
* email:
*	luispizano@iteso.mx
* Date:
*	01/03/2014
******************************************************************/
module Control
(
	input [5:0]OP,
	
	output RegDst,
	output BranchEQ,
	output BranchNE,
	output MemRead,
	output MemtoReg,
	output MemWrite,
	output ALUSrc,
	output RegWrite,
	//Se añaden señales para controlar los nuevos modulos del datapath
	output [2:0]ALUOp,
	output ZeroOrSignExt,
	output Jump,
	output JAL
);
localparam R_Type = 0;
localparam I_Type_ADDI = 6'h8;
localparam I_Type_ORI = 6'h0d;
//Se especifican los codigos de las instrucciones faltantes.
localparam I_Type_LUI = 6'h0f;
localparam I_Type_ANDI = 6'h0c;
localparam I_Type_LW = 6'h23;
localparam I_Type_SW = 6'h2b;
localparam BEQ = 6'h4;
localparam BNE = 6'h5;
localparam J_Type_J  = 6'h02;
localparam J_Type_JAL  = 6'h03;

//Se crecel bus de instruccion
reg [14:0] ControlValues;

always@(OP) begin
	casex(OP)
	//Se completan las nuevas señales para las instrucciones que ya existian
		R_Type:       ControlValues= 15'b00_01_001_00_00_111;
		I_Type_ADDI:  ControlValues= 15'b00_00_101_00_00_100;
		I_Type_ORI:   ControlValues= 15'b00_10_101_00_00_101;
		// Se añaden las señales de control para las nuevas instrucciones
		I_Type_LUI:   ControlValues= 15'b00_00_101_00_00_000; ////////////////////////////
		I_Type_ANDI:  ControlValues= 15'b00_10_101_00_00_110;
		I_Type_LW:	  ControlValues= 15'b00_00_111_10_00_010;
		I_Type_SW:	  ControlValues= 15'b00_00_1X0_01_00_011;
		BEQ:  		  ControlValues= 15'b00_00_0X0_00_01_001;
		BNE:  		  ControlValues= 15'b00_00_0X0_00_10_001;
		J_Type_J:	  ControlValues= 15'b01_XX_XX0_00_00_XXX;
		J_Type_JAL:	  ControlValues= 15'b11_XX_XX1_00_00_XXX;
		default:
			ControlValues= 15'b0000000000;
		endcase
end	


// Se realiza la asignacion del bus de control a señales individuales
assign JAL = ControlValues[13];
assign Jump = ControlValues[12];
assign ZeroOrSignExt = ControlValues[11];
assign RegDst = ControlValues[10];
assign ALUSrc = ControlValues[9];
assign MemtoReg = ControlValues[8];
assign RegWrite = ControlValues[7];
assign MemRead = ControlValues[6];
assign MemWrite = ControlValues[5];
assign BranchNE = ControlValues[4];
assign BranchEQ = ControlValues[3];
assign ALUOp = ControlValues[2:0];	

endmodule


