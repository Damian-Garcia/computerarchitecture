/******************************************************************
* Description
*	This module performes a sign-extend operation that is need with
*	in instruction like andi or ben.
* Version:
*	1.0
* Author:
*	Dr. José Luis Pizano Escalante
* email:
*	luispizano@iteso.mx
* Date:
*	01/03/2014
******************************************************************/
module ExtendConstants
(   
	input [15:0]  DataInput,
	input ZeroOrSignExt,
   output[31:0] ExtendConstantsOutput
);

wire [31:0]SignExtendOutput;
wire [31:0] ZeroExtendOutput;

//Se determina con una señal si se quiere realizar la extension de signo o de ceros (aritmeticas o logicas)
assign  SignExtendOutput = {{16{DataInput[15]}},DataInput[15:0]};
assign ZeroExtendOutput =  {{16{1'b0}}, DataInput[15:0]};

assign ExtendConstantsOutput = (ZeroOrSignExt)? ZeroExtendOutput : SignExtendOutput;

endmodule // signExtend
