/******************************************************************
* Description
*	This is an 32-bit arithetic logic unit that can execute the next set of operations:
*		add
*		sub
*		or
*		and
*		nor
* This ALU is written by using behavioral description.
* Version:
*	1.0
* Author:
*	Dr. José Luis Pizano Escalante
* email:
*	luispizano@iteso.mx
* Date:
*	01/03/2014
******************************************************************/

module ALU 
(
	input [3:0] ALUOperation,
	input [31:0] A,
	input [31:0] B,
	input [4:0] Shamt, //La ALU tambien recibira como parametro la cantidad de bits de corrimiento en las instrucciones R
	output reg Zero,
	output reg [31:0]ALUResult
);
localparam AND = 4'b0000;
localparam OR  = 4'b0001;
localparam NOR = 4'b0010;
localparam ADD = 4'b0011;
localparam SUB = 4'b0100;
localparam RL = 4'b0101;
localparam RR = 4'b0110;
localparam LUI = 4'b0111;
   
  //Se crean wires para almacenar el valor del shift
reg [31:0]shiftedL;
reg [31:0]shiftedR;

//Se calcula el resultado del shift dependiendo de shamt
always@(Shamt or B) begin
	case (Shamt)
		5'd0:
			shiftedL = B << 0;
		5'd1:
			shiftedL = B << 1;
		5'd2:
			shiftedL = B << 2;
		5'd3:
			shiftedL = B << 3;
		5'd4:
			shiftedL = B << 4;
		5'd5:
			shiftedL = B << 5;
		5'd6:
			shiftedL = B << 6;
		5'd7:
			shiftedL = B << 7;
		5'd8:
			shiftedL = B << 8;
		5'd9:
			shiftedL = B << 9;
		5'd10:
			shiftedL = B << 10;
		5'D11:
			shiftedL = B << 11;
		5'd12:
			shiftedL = B << 12;
		5'd13:
			shiftedL = B << 13;
		5'd14:
			shiftedL = B << 14;
		5'd15:
			shiftedL = B << 15;
		5'd16:
			shiftedL = B << 16;
		5'd17:
			shiftedL = B << 17;
		5'd18:
			shiftedL = B << 18;
		5'd19:
			shiftedL = B << 19;
		5'd20:
			shiftedL = B << 20;
		5'd21:
			shiftedL = B << 21;
		5'd22:
			shiftedL = B << 22;
		5'd23:
			shiftedL = B << 23;
		5'd24:
			shiftedL = B << 24;
		5'd25:
			shiftedL = B << 25;
		5'd26:
			shiftedL = B << 26;
		5'd27:
			shiftedL = B << 27;
		5'd28:
			shiftedL = B << 28;
		5'd29:
			shiftedL = B << 29;
		5'd30:
			shiftedL = B << 30;
		5'd31:
			shiftedL = B << 31;
		default:
			shiftedL = B;
	endcase
end

//Se realiza lo mismo para ambos sentidos de rotacion
always@(Shamt or B) begin
	case (Shamt)
		5'd0:
			shiftedR = B >> 0;
		5'd1:
			shiftedR = B >> 1;
		5'd2:
			shiftedR = B >> 2;
		5'd3:
			shiftedR = B >> 3;
		5'd4:
			shiftedR = B >> 4;
		5'd5:
			shiftedR = B >> 5;
		5'd6:
			shiftedR = B >> 6;
		5'd7:
			shiftedR = B >> 7;
		5'd8:
			shiftedR = B >> 8;
		5'd9:
			shiftedR = B >> 9;
		5'd10:
			shiftedR = B >> 10;
		5'D11:
			shiftedR = B >> 11;
		5'd12:
			shiftedR = B >> 12;
		5'd13:
			shiftedR = B >> 13;
		5'd14:
			shiftedR = B >> 14;
		5'd15:
			shiftedR = B >> 15;
		5'd16:
			shiftedR = B >> 16;
		5'd17:
			shiftedR = B >> 17;
		5'd18:
			shiftedR = B >> 18;
		5'd19:
			shiftedR = B >> 19;
		5'd20:
			shiftedR = B >> 20;
		5'd21:
			shiftedR = B >> 21;
		5'd22:
			shiftedR = B >> 22;
		5'd23:
			shiftedR = B >> 23;
		5'd24:
			shiftedR = B >> 24;
		5'd25:
			shiftedR = B >> 25;
		5'd26:
			shiftedR = B >> 26;
		5'd27:
			shiftedR = B >> 27;
		5'd28:
			shiftedR = B >> 28;
		5'd29:
			shiftedR = B >> 29;
		5'd30:
			shiftedR = B >> 30;
		5'd31:
			shiftedR = B >> 31;
		default
			shiftedR = B;
	endcase
end
	

   always @ (A or B or ALUOperation or shiftedL or shiftedR)
     begin
		case (ALUOperation)
		  ADD: // add
			ALUResult=A + B;
		  SUB: // sub
			ALUResult=A - B;
		  AND: // and
			ALUResult= A & B;
		  OR: // or
			ALUResult= A | B;
		  NOR: // or
			ALUResult= ~(A|B);
		//Se agregan dos nuevas operaciones que puede realizar la ALU
			RL: 
		   ALUResult = shiftedL;
		  RR:
			ALUResult = shiftedR;
		  LUI:
			ALUResult = B << 16;
		default:
			ALUResult= 0;
		endcase // case(control)
		Zero = (ALUResult==0) ? 1'b1 : 1'b0;
     end // always @ (A or B or control)
endmodule // ALU