/******************************************************************
* Description
*	This is the top-level of a MIPS processor that can execute the next set of instructions:
*		add
*		addi
*		sub
*		ori
*		or
*		bne
*		beq
*		and
*		nor
* This processor is written Verilog-HDL. Also, it is synthesizable into hardware.
* Parameter MEMORY_DEPTH configures the program memory to allocate the program to
* be execute. If the size of the program changes, thus, MEMORY_DEPTH must change.
* This processor was made for computer organization class at ITESO.
* Version:
*	1.0
* Author:
*	Dr. José Luis Pizano Escalante
* email:
*	luispizano@iteso.mx
* Date:
*	12/06/2016
******************************************************************/


module MIPS_Processor
#(
	parameter MEMORY_DEPTH = 512
)

(
	// Inputs
	input clk,
	input reset,
	//input [7:0] PortIn,
	// Output
	output [31:0] ALUResultOut
	//output [31:0] PortOut
);
//******************************************************************/
//******************************************************************/
//assign  PortOut = 0;

//******************************************************************/
//******************************************************************/
// Data types to connect modules
wire BranchNE_wire;
wire BranchEQ_wire;
wire RegDst_wire;
wire NotZeroANDBrachNE;
wire ZeroANDBrachEQ;
wire ORForBranch;
wire ALUSrc_wire;
wire RegWrite_wire;
wire Zero_wire;
//Se añaden las nuevas señales de control
wire DataSource_wire;
wire ZeroOrSignExt_wire;
wire MemRead_wire;
wire MemToReg_wire;
wire MemWrite_wire;
wire Jump_wire;
wire JAL_wire;
wire JR_wire;
wire [2:0] ALUOp_wire;
wire [3:0] ALUOperation_wire;
wire [4:0] WriteRegister_wire;
wire [31:0] MUX_PC_wire;
wire [31:0] PC_wire;
wire [31:0] Instruction_wire;
wire [31:0] ReadData1_wire;
wire [31:0] ReadData2_wire;
wire [31:0] InmmediateExtend_wire;
wire [31:0] ReadData2OrInmmediate_wire;
wire [31:0] ALUResult_wire;
wire [31:0] PC_4_wire;
wire [31:0] InmmediateExtendAnded_wire;
wire [31:0] PCtoBranch_wire;
//Se añaden nuevos buses al datapath
wire [31:0] WriteData_wire;
wire [31:0] DataMemoryAddress_wire;
wire [31:0] DataMemoryWriteData_wire;
wire [31:0] DataMemoryReadData_wire;
wire [31:0] ALUOrMemoryMux_wire;
wire [31:0] InmediateExtendShift_wire;
wire [31:0] CalculatedBranchAddress_wire;
wire [31:0] NextPCOrBranch;
wire [31:0] JTypeAddressShifted;
wire [31:0] NextPCOrJump_wire;
wire [31:0] DataMemoryAddressOffset_wire;
wire [4:0] InstructionSelectedRegister_wire;
wire [31:0] OperationData_wire;
wire [31:0] NextPcOrRegister_wire;
integer ALUStatus;

wire PCSrc_B;

/*********Wires de pipeline*****************************************/
//IF ID
wire [31:0] IF_ID_PC_4_out;
wire [31:0] IF_ID_instruction_out;
//ID EX
wire [1:0] ID_EX_WB_out;
wire [3:0] ID_EX_M_out;
wire [4:0] ID_EX_EX_out;
wire [31:0] ID_EX_PC_4_out;
wire [31:0] ID_EX_readData1_out;
wire [31:0] ID_EX_readData2_out;
wire [31:0] ID_EX_extendedConstant_out;
wire [4:0] ID_EX_RegisterRS_out;
wire [4:0] ID_EX_RegisterRT_out;
wire [4:0] ID_EX_RegisterRD_out;
wire [4:0] ID_EX_Shamt_out;
wire [5:0] ID_EX_ALUFunc_out;
//EX MEM
wire [1:0]	EX_MEM_WB_out;
wire [3:0]	EX_MEM_M_out;
wire [31:0]	EX_MEM_BranchAddr_out;
wire 	EX_MEM_ZeroFlag_out;
wire [31:0]	EX_MEM_ALUResult_out;
wire [31:0]	EX_MEM_WriteData_out;
wire [4:0]	EX_MEM_SelectedDest_out;
//MEM_WB
wire [1:0] MEM_WB_WB_out;
wire [31:0] MEM_WB_ReadData_out;
wire [31:0] MEM_WB_ALUResult_out;
wire [4:0] MEM_WB_SelectedDest_out;
////////////////////////////////////////////////////////////////////

//******************************************************************/
//******************************************************************/
//******************************************************************/
//******************************************************************/
//******************************************************************/
Control
ControlUnit
(
	.OP(IF_ID_instruction_out[31:26]),
	.RegDst(RegDst_wire),
	.BranchNE(BranchNE_wire),
	.BranchEQ(BranchEQ_wire),
	//Se especifican las señales de cotrol como salidas de la unidad de control
	.MemRead(MemRead_wire),
	.MemtoReg(MemToReg_wire),
	.MemWrite(MemWrite_wire),
	.ALUOp(ALUOp_wire),
	.ALUSrc(ALUSrc_wire),
	.RegWrite(RegWrite_wire),
	.ZeroOrSignExt(ZeroOrSignExt_wire),
	.Jump(Jump_wire),
	.JAL(JAL_wire)
);

PC_Register
ProgramCounter
(
	.clk(clk),
	.reset(reset),
	//El siguiente PC puede ser PC+4, branch address, jump address o un registro
	.NewPC(NextPCOrBranch),
	.PCValue(PC_wire)
);


ProgramMemory
#(
	.MEMORY_DEPTH(MEMORY_DEPTH)
)
ROMProgramMemory
(
	.Address(PC_wire),
	.Instruction(Instruction_wire)
);

Adder32bits
PC_Puls_4
(
	.Data0(PC_wire),
	.Data1(4),
	
	.Result(PC_4_wire)
);


//******************************************************************/
//******************************************************************/
//******************************************************************/
//******************************************************************/
//******************************************************************/
Multiplexer2to1
#(
	.NBits(5)
)
MUX_ForRTypeAndIType
(
	.Selector( ID_EX_EX_out[2] ),
	.MUX_Data0( ID_EX_RegisterRT_out ),
	.MUX_Data1( ID_EX_RegisterRD_out ),
	//Este mux puede seleccionar el registro fuente para una instrucción I o R.
	.MUX_Output(InstructionSelectedRegister_wire)

);



RegisterFile
Register_File
(
	.clk(clk),
	.reset(reset),
	.RegWrite(MEM_WB_WB_out[1]),
	//Lo quee se va a escribir en el RF puede ser el resultado de una operacion de la ALU, de la memoria o al carga de un inmediato
	.WriteRegister(MEM_WB_SelectedDest_out),
	.ReadRegister1(IF_ID_instruction_out[25:21]),
	.ReadRegister2(IF_ID_instruction_out[20:16]),
	.WriteData(ALUOrMemoryMux_wire),
	.ReadData1(ReadData1_wire),
	.ReadData2(ReadData2_wire)

);

//Se cambia el nombre de SignExtend, ahora puede propagar el bit de signo o concatenar ceros
ExtendConstants
SignExtendForConstants
(   
	.DataInput(IF_ID_instruction_out[15:0]),
	.ZeroOrSignExt(ZeroOrSignExt_wire),
   .ExtendConstantsOutput(InmmediateExtend_wire)
);



Multiplexer2to1
#(
	.NBits(32)
)
MUX_ForReadDataAndInmediate
(
	.Selector( ID_EX_EX_out[0] ),
	.MUX_Data0(ID_EX_readData2_out),
	.MUX_Data1(ID_EX_extendedConstant_out),
	
	.MUX_Output(ReadData2OrInmmediate_wire)

);


ALUControl
ArithmeticLogicUnitControl
(
	.ALUOp( ID_EX_EX_out[3:1] ),
	.ALUFunction( ID_EX_ALUFunc_out ),
	.ALUOperation(ALUOperation_wire),
	//Señal de control especifica para controlar el datapath en el caso de una JR.
	.JR(JR_wire)
);



ALU
ArithmeticLogicUnit 
(
	.ALUOperation(ALUOperation_wire),
	.A(ID_EX_readData1_out),
	.B(ReadData2OrInmmediate_wire),
	//ALU ahora recibie el shamtpara hacer corrimientos
	.Shamt( ID_EX_Shamt_out),
	.Zero(Zero_wire),
	.ALUResult(ALUResult_wire)
);


//MUX para seleccionar entre ALU o una operacion de cargar de memoria
Multiplexer2to1 #(.NBits(32))
ALUOrMemoryMux(
	.Selector(MEM_WB_WB_out[0]),
	.MUX_Data0(MEM_WB_ALUResult_out),
	.MUX_Data1(MEM_WB_ReadData_out),
	
	.MUX_Output(ALUOrMemoryMux_wire)

);


//Calculador de Offset para la memoria de datos,
Adder32bits
DataAddressOffsetCalculator
(
	.Data0(EX_MEM_ALUResult_out),
	.Data1(32'hEFFF0000),
	
	.Result(DataMemoryAddressOffset_wire)
);

//Se agrega la memoria de datos
DataMemory 
#(	.DATA_WIDTH(32),
	.MEMORY_DEPTH(512)
)
MemoriaDatos
(
	.WriteData(EX_MEM_WriteData_out),
	.Address({2'b0,  DataMemoryAddressOffset_wire[31:2]}),
	.MemWrite(EX_MEM_M_out[0]),
	.MemRead(EX_MEM_M_out[1]), 
	.clk(clk),
	.ReadData(DataMemoryReadData_wire)
);

//Se agrega un shift al dato inmediato,para usarlo en branches
ShiftLeft2 shift2leftExtended
(   
	.DataInput(ID_EX_extendedConstant_out),
   .DataOutput(InmediateExtendShift_wire)
);

//Se suma el offset del dato inmediato con PC+4 para realizar branches
Adder32bits
BranchAddressCalculator
(
	.Data0(ID_EX_PC_4_out),
	.Data1(InmediateExtendShift_wire),
	
	.Result(CalculatedBranchAddress_wire)
);

BranchController bc
(
	.BEQ(EX_MEM_M_out[2]),
	.BNE(EX_MEM_M_out[3]),
	.Zero(EX_MEM_ZeroFlag_out),
	.PCSrc_B(  )
);

//Selecciona entre PC+4 (siguiente instruccion) o la direccion de un branch
Multiplexer2to1 #(.NBits(32))
NextPCOrBranchMux(
	.Selector(PCSrc_B),
	.MUX_Data0(PC_4_wire),
	.MUX_Data1(EX_MEM_BranchAddr_out),
	
	.MUX_Output(NextPCOrBranch)

);

//Shift de la direccion de una instruccion tipo J para realizar direccionamiento pseudodirecto
ShiftLeft2 shift2leftJType
(   
	.DataInput({6'b0, Instruction_wire[25:0]}),
   .DataOutput(JTypeAddressShifted)
);

//Multiplexor para seleccionar entre (PC+4 o BrachAddr) o la direccion de salto de un J
Multiplexer2to1 #(.NBits(32))
NextPCOrJumpMux(
	.Selector(Jump_wire),
	.MUX_Data0(NextPCOrBranch),
	.MUX_Data1({PC_4_wire[31:28],JTypeAddressShifted[27:0]}),
	
	.MUX_Output(NextPCOrJump_wire)

);

//Multiplexor para sseleccionar entre direccionar el registro RA en el RF o el registro seleccionado por una instruccion
Multiplexer2to1 #(.NBits(5))
Reg_Or_RA_Mux(
	.Selector(JAL_wire),
	.MUX_Data0(InstructionSelectedRegister_wire),
	.MUX_Data1(5'd31),
	
	.MUX_Output(WriteRegister_wire)

);

// Multiplexor para seleccionar la entrada del RF, que sea el PC+4 o el resultado de una operacion 
Multiplexer2to1 #(.NBits(32))
DataOrPC4_MUX(
	.Selector(JAL_wire),
	.MUX_Data0(ALUOrMemoryMux_wire),
	.MUX_Data1(PC_4_wire),
	.MUX_Output(WriteData_wire)
);

// Multiplexor para seleccionar el siguiente PC, ya sea el calculado, o el resultado de hacer JR
Multiplexer2to1 #(.NBits(32))
NextPCorRegisterMux(
	.Selector(JR_wire),
	.MUX_Data0(NextPCOrJump_wire),
	.MUX_Data1(ReadData1_wire),
	.MUX_Output(NextPcOrRegister_wire)
);

IF_ID if_id_instance
(
	.clk(clk),
	.reset(reset),
	.enable(1'b1),
	.PC_4_in(PC_4_wire),
	.instruction_in(Instruction_wire),
	.PC_4_out(IF_ID_PC_4_out),
	.instruction_out(IF_ID_instruction_out)
);

ID_EX id_ex_instance
(
	.clk(clk),
	.reset(reset),
	.enable(1'b1),

	.WB_in({RegWrite_wire,MemToReg_wire}),
	.M_in( {BranchNE_wire, BranchEQ_wire, MemRead_wire, MemWrite_wire}),
	.EX_in({ RegDst_wire, ALUOp_wire, ALUSrc_wire }),
	.PC_4_in(IF_ID_PC_4_out),
	.readData1_in( ReadData1_wire ),
	.readData2_in( ReadData2_wire ),
	.extendedConstant_in( InmmediateExtend_wire ),
	.RegisterRS_in( IF_ID_instruction_out[25:21] ),
	.RegisterRT_in( IF_ID_instruction_out[20:16]),
	.RegisterRD_in( IF_ID_instruction_out[15:11] ),
	.Shamt_in(IF_ID_instruction_out[10:6]),
	.ALUFunc_in(IF_ID_instruction_out[5:0]),
	
	.WB_out(ID_EX_WB_out),
	.M_out(ID_EX_M_out),
	.EX_out(ID_EX_EX_out),
	.PC_4_out(ID_EX_PC_4_out),
	.readData1_out(ID_EX_readData1_out),
	.readData2_out(ID_EX_readData2_out),
	.extendedConstant_out(ID_EX_extendedConstant_out),
	.RegisterRS_out(ID_EX_RegisterRS_out),
	.RegisterRT_out(ID_EX_RegisterRT_out),
	.RegisterRD_out(ID_EX_RegisterRD_out),
	.Shamt_out(ID_EX_Shamt_out),
	.ALUFunc_out(ID_EX_ALUFunc_out)
	);
	
	
 EX_MEM ex_mem_instance
(
	.clk(clk),
	.reset(reset),
	.enable(1'b1),
	
	.WB_in(ID_EX_WB_out),
	.M_in(ID_EX_M_out),
	.BranchAddr_in(CalculatedBranchAddress_wire),
	.ZeroFlag_in(Zero_wire),
	.ALUResult_in(ALUResult_wire),
	.WriteData_in(ID_EX_readData2_out),
	.SelectedDest_in(InstructionSelectedRegister_wire),
	
	.WB_out(EX_MEM_WB_out),
	.M_out(EX_MEM_M_out),

	.BranchAddr_out(EX_MEM_BranchAddr_out),
	.ZeroFlag_out(EX_MEM_ZeroFlag_out),
	.ALUResult_out(EX_MEM_ALUResult_out),
	.WriteData_out(EX_MEM_WriteData_out),
	.SelectedDest_out(EX_MEM_SelectedDest_out)
);

MEM_WB mem_wb_instance
(
	.clk(clk),
	.reset(reset),
	.enable(1'b1),
	
	.WB_in( EX_MEM_WB_out ),
	.ReadData_in( DataMemoryReadData_wire ),
	.ALUResult_in( EX_MEM_ALUResult_out ),
	.SelectedDest_in (EX_MEM_SelectedDest_out ),
	
	.WB_out(MEM_WB_WB_out),
	.ReadData_out(MEM_WB_ReadData_out),
	.ALUResult_out(MEM_WB_ALUResult_out),
	.SelectedDest_out(MEM_WB_SelectedDest_out)
	);
	
assign ALUResultOut = ALUResult_wire;

endmodule

