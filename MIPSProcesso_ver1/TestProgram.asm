.data
	numero: .word 1, 2
	
.text
	# Initializations

jal 	func
	add $s0, $zero, $zero

func:
	addi $t1, $t1, 1
	lui $t2, 0x1001
	ori $t2, 0
	sw $t1, ($t2)
	lw $t3, ($t2)
	j func
