module IF_ID
(
	input clk,
	input reset,
	input enable,
	input  [31:0] PC_4_in,
	input [31:0] instruction_in,
	output reg [31:0] PC_4_out,
	output reg [31:0] instruction_out
);

always@(negedge clk ) begin
	if(reset==0) begin
		PC_4_out <= 0;
		instruction_out <= 0;
		end
	else	
		if(enable==1) begin
			instruction_out<=instruction_in;
			PC_4_out <= PC_4_in;
		end
end

endmodule