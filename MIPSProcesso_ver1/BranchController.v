module BranchController
(
	input BEQ,
	input BNE,
	input Zero,
	output PCSrc_B
);

assign PCSrc_B = (Zero && BEQ) || (!Zero && BNE);

endmodule
